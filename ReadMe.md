# UE4 C++ 快速入门

如何在虚幻引擎中设置C++项目及在Visual Studio中编写首个C++ gameplay类。

## 创建并编辑 C++ gameplay 类 FloatingActor

```cpp

UPROPERTY(VisibleAnywhere)
UStaticMeshComponent* VisualMesh;

// Sets default values
AFloatingActor::AFloatingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VisualMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualMesh"));
	//not to registered
	VisualMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	if (CubeVisualAsset.Succeeded()) {
		VisualMesh->SetStaticMesh(CubeVisualAsset.Object);
		VisualMesh->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
	}
}

```
## 使FloatingActor上下浮动的同时旋转

```cpp


UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Floating Actor")
float FloatingSpeed=20.0f;

UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Floating Actor")
float RotationSpeed=20.0f;

// Called every frame
void AFloatingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector NewLocation = GetActorLocation();
	FRotator NewRotation = GetActorRotation();

	float RunningTime = GetGameTimeSinceCreation();

	float DeltaHeight = FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime);
	NewLocation.Z += DeltaHeight * FloatingSpeed;

	NewRotation.Yaw += DeltaTime * RotationSpeed;

	SetActorLocationAndRotation(NewLocation, NewRotation);
}
```

## Note

创建和附加对象和组件。

### 创建组件

```cpp
// 查找组件
auto directionalLightActor = UGameplayStatics::GetActorOfClass(GetWorld(), ADirectionalLight::StaticClass());

if (directionalLightActor)
{
	//Don't save object 
	auto directionalLightReposition = NewObject<UArcGISDirectionalLightRepositionComponent>(
		directionalLightActor, UArcGISDirectionalLightRepositionComponent::StaticClass(), NAME_None, RF_Transient);
	//注册
	directionalLightReposition->RegisterComponent();
}
```

### 附加组件

```cpp
//SetupAttachment 应该只用于为未来的 AttachToComponent 初始化 AttachParent 和 AttachSocketName。注册组件后，必须使用 AttachToComponent。
void USceneComponent::SetupAttachment(class USceneComponent* InParent, FName InSocketName){
	//SetupAttachment should only be used to initialize AttachParent and AttachSocketName for a future AttachToComponent. Once a component is registered you must use AttachToComponent.
}

// Attach this component to another scene component, optionally at a named socket. It is valid to call this on components whether or not they have been Registered, however from constructor or when not registered it is preferable to use SetupAttachment
// 将此组件附加到另一个场景组件，可选择在命名的socket上。无论是否已注册，在组件上调用它都是有效的，但是从构造函数或未注册时，最好使用 SetupAttachment。
bool USceneComponent::AttachToComponent(USceneComponent* Parent, const FAttachmentTransformRules& AttachmentRules, FName SocketName){

}
```


## Result

![QuickStart](./Images/QuickStart.png)



![FloatingActor](./Images/FloatingActor.png)



## Reference

1. [UE4 C++ 编程快速入门 - https://docs.unrealengine.com/4.26/zh-CN/ProgrammingAndScripting/ProgrammingWithCPP/CPPProgrammingQuickStart/](https://docs.unrealengine.com/4.26/zh-CN/ProgrammingAndScripting/ProgrammingWithCPP/CPPProgrammingQuickStart/)